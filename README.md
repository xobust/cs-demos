## Adding a demo

1. Copy/paste the demo in the `csgo` or `cs2` folder
2. `git add .`
3. `git ls-files | grep .dem > demos.txt`
4. Commit and push
